package org.next.door.service.order;

import javax.inject.Inject;

import org.next.door.dao.order.OrderDAO;
import org.next.door.model.account.Account;
import org.next.door.model.order.Order;
import org.next.door.model.paging.Paging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
@Service
public class OrderServiceImpl implements OrderService {

	/**
	 * Log events
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final OrderDAO orderDAO;
	
	/**
	 * Initialise OrderService
	 * 
	 * @param orderDAO
	 */
	@Inject
	public OrderServiceImpl(OrderDAO orderDAO) {
		this.orderDAO = orderDAO;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Order> createOrder(Order order) {
		logger.info("createOrder(Order order) arg : {}", order);
		return Observable.<Order>create (source -> {
			try {
				source.onNext(orderDAO.createAndFlush(order));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Order> updateOrder(Order order) {
		logger.info("updateOrder(Order order) arg : {}", order);
		return Observable.<Order>create (source -> {
			try {
				source.onNext(orderDAO.updateAndFlush(order));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Order> readOrder(Long id) {
		logger.info("readOrder(Long id) arg : {}", id);
		return Observable.<Order>create (source -> {
			try {
				source.onNext(orderDAO.get(Order.class, id));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Order> listOrder(Paging paging, Account account) {
		logger.info("listOrder(Paging paging, Account account) arg : {}, {}", paging, account);
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Order> removeOrder(Long id) {
		logger.info("removeOrder(Long id) arg : {}", id);
		return Observable.<Order>create (source -> {
			try {
				Order order = orderDAO.get(Order.class, id);
				orderDAO.delete(order);
				source.onNext(order);
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

}
