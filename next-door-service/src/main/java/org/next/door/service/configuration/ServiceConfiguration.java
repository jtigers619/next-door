package org.next.door.service.configuration;

import org.next.door.dao.configuration.DatabaseConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * @author ad
 *
 */
@Configuration
@Import(value = { DatabaseConfiguration.class })
@ComponentScan(basePackages = {
		"org.next.door.service" })
public class ServiceConfiguration {

	/**
	 * An object assignable to Spring's BeanFactoryPostProcessor
	 * interface(implementations of BeanFactoryPostProcessor) should be declared
	 * as static if @Bean annotation is present. This will result in a failure
	 * to process annotations such as
	 * 
	 * @Autowired, @Resource and @PostConstruct within the method's
	 * declaring @Configuration class. Add the 'static' modifier to this method
	 * to avoid these container lifecycle issues; see @Bean javadoc for complete
	 * details
	 * 
	 * @return PropertySourcesPlaceholderConfigurer
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	/**
	 * Register {@link MessageSource} get messages from property files.
	 * 
	 * @return {@link MessageSource} - 
	 * 									for internationalization of i18 messages.
	 */
	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasenames("/META-INF/i18n/messages", "/META-INF/i18n/validation-messages");
		messageSource.setUseCodeAsDefaultMessage(Boolean.TRUE);
		return messageSource;
	}
}
