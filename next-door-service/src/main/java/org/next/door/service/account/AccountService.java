package org.next.door.service.account;

import org.next.door.model.account.Account;

import io.reactivex.Observable;


/**
 * @author ad
 *
 */
public interface AccountService {
	
	/**
	 * Create Account
	 * 
	 * @param account
	 * @return Account
	 */
	Observable<Account> createAccount(Account account);
	
	/**
	 * Update Account
	 * 
	 * @param account
	 * @return Account
	 */
	Observable<Account> updateAccount(Account account);
	
	/**
	 * Read Account
	 * 
	 * @param email
	 * @return Account
	 */
	Observable<Account> readAccount(String email);
	
	/**
	 * List Account
	 * 
	 * @return List<Account>
	 */
	Observable<Account> listAccount();
	
	/**
	 * Remove Account
	 * 
	 * @param id
	 * @return Account
	 */
	Observable<Account> removeAccount(Long id);
}
