package org.next.door.service.order;

import javax.validation.constraints.NotNull;

import org.next.door.model.account.Account;
import org.next.door.model.order.Order;
import org.next.door.model.paging.Paging;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
public interface OrderService {

	/**
	 * Create Order
	 * 
	 * @param order
	 * @return Observable<Order>
	 */
	Observable<Order> createOrder(Order order);
	
	/**
	 * Update Order
	 * 
	 * @param Order
	 * @return Observable<Order>
	 */
	Observable<Order> updateOrder(Order order);
	
	/**
	 * Read Order
	 * 
	 * @param id
	 * @return Observable<Order>
	 */
	Observable<Order> readOrder(Long id);
	
	/**
	 * Get list of {@link Order}
	 * 
	 * @param paging
	 * @param account
	 * @return Observable<Order>
	 */
	Observable<Order> listOrder(@NotNull Paging paging, Account account);
	
	/**
	 * Remove Order
	 * 
	 * @param id
	 * @return Order
	 */
	Observable<Order> removeOrder(Long id);
}
