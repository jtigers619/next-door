package org.next.door.service.account;

import javax.inject.Inject;

import org.next.door.dao.account.AccountDAO;
import org.next.door.model.account.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.reactivex.Observable;


/**
 * @author ad
 *
 */
@Service
public class AccountServiceImpl implements AccountService {
	
	/**
	 * Log events
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private final AccountDAO accountDAO;
	
	/**
	 * Initialize dependency
	 * 
	 * @param accountDAO
	 */
	@Inject
	public AccountServiceImpl (AccountDAO accountDAO) {
		this.accountDAO = accountDAO;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Account> createAccount(Account account) {
		logger.info("createAccount(Account account) arg : {}", account);
		return Observable.<Account>create (source -> {
			try {
				source.onNext(accountDAO.createAndFlush(account));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Account> updateAccount(Account account) {
		logger.info("updateAccount(Account account) arg : {}", account);
		return Observable.<Account>create (source -> {
			try {
				source.onNext(accountDAO.updateAndFlush(account));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Account> readAccount(String email) {
		logger.info("readAccount(String email) arg : {}", email);
		return accountDAO.readByEmail(email);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Account> listAccount() {
		logger.info("listAccount()");
		return accountDAO.list();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Account> removeAccount(Long id) {
		logger.info("removeAccount(Long id) arg : {}", id);
		return Observable.<Account>create (source -> {
			try {
				Account account = accountDAO.get(Account.class, id);
				source.onNext(account);
				accountDAO.delete(account);
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

}
