package org.next.door.service.cart;

import javax.validation.constraints.NotNull;

import org.next.door.model.account.Account;
import org.next.door.model.cart.Cart;
import org.next.door.model.paging.Paging;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
public interface CartService {
	
	/**
	 * Create Cart
	 * 
	 * @param cart
	 * @return Observable<Cart>
	 */
	Observable<Cart> createCart(Cart cart);
	
	/**
	 * Update Cart
	 * 
	 * @param Cart
	 * @return Observable<Cart>
	 */
	Observable<Cart> updateCart(Cart cart);
	
	/**
	 * Read Cart
	 * 
	 * @param id
	 * @return Observable<Cart>
	 */
	Observable<Cart> readCart(Long id);
	
	/**
	 * Get list of {@link Cart}
	 * 
	 * @param paging
	 * @param account
	 * @return Observable<Cart>
	 */
	Observable<Cart> listCart(@NotNull Paging paging, Account account);
	
	/**
	 * Remove Cart
	 * 
	 * @param id
	 * @return Cart
	 */
	Observable<Cart> removeCart(Long id);
}
