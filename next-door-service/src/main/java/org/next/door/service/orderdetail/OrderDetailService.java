package org.next.door.service.orderdetail;

import javax.validation.constraints.NotNull;

import org.next.door.model.account.Account;
import org.next.door.model.orderdetail.OrderDetail;
import org.next.door.model.paging.Paging;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
public interface OrderDetailService {

	/**
	 * Create OrderDetail
	 * 
	 * @param orderDetail
	 * @return Observable<OrderDetail>
	 */
	Observable<OrderDetail> createOrderDetail(OrderDetail orderDetail);
	
	/**
	 * Update OrderDetail
	 * 
	 * @param OrderDetail
	 * @return Observable<OrderDetail>
	 */
	Observable<OrderDetail> updateOrderDetail(OrderDetail orderDetail);
	
	/**
	 * Read OrderDetail
	 * 
	 * @param id
	 * @return Observable<OrderDetail>
	 */
	Observable<OrderDetail> readOrderDetail(Long id);
	
	/**
	 * Get list of {@link OrderDetail}
	 * 
	 * @param paging
	 * @param account
	 * @return Observable<OrderDetail>
	 */
	Observable<OrderDetail> listOrderDetail(@NotNull Paging paging, Account account);
	
	/**
	 * Remove OrderDetail
	 * 
	 * @param id
	 * @return OrderDetail
	 */
	Observable<OrderDetail> removeOrderDetail(Long id);
}
