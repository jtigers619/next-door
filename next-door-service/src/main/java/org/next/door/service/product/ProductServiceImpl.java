package org.next.door.service.product;

import javax.inject.Inject;

import org.next.door.dao.product.ProductDAO;
import org.next.door.model.paging.Paging;
import org.next.door.model.product.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
@Service
public class ProductServiceImpl implements ProductService {

	/**
	 * Log events
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final ProductDAO productDAO;
	
	/**
	 * Initialize product service.
	 * 
	 * @param productDAO
	 */
	@Inject
	public ProductServiceImpl (ProductDAO productDAO) {
		this.productDAO = productDAO;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Product> createProduct(Product product) {
		logger.info("createProduct(Product Product) arg : {}", product);
		return Observable.create(source -> {
			try {
				source.onNext(productDAO.createAndFlush(product));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Product> updateProduct(Product product) {
		logger.info("updateProduct(Product Product) arg : {}", product);
		return Observable.create(source -> {
			try {
				source.onNext(productDAO.updateAndFlush(product));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Product> readProduct(Long id) {
		logger.info("readProduct(Long id) arg : {}", id);
		return Observable.create(source -> {
			try {
				source.onNext(productDAO.get(Product.class, id));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Product> listProduct(Paging paging) {
		logger.info("listProduct(Paging paging) arg : {}", paging);
		return productDAO.list(paging);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Product> removeProduct(Long id) {
		logger.info("removeProduct(Long id) arg : {}", id);
		return Observable.create(source -> {
			try {
				Product product = productDAO.get(Product.class, id);
				productDAO.delete(product);
				source.onNext(product);
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

}
