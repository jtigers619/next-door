package org.next.door.service.cart;

import javax.inject.Inject;

import org.next.door.dao.cart.CartDAO;
import org.next.door.model.account.Account;
import org.next.door.model.cart.Cart;
import org.next.door.model.paging.Paging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
@Service
public class CartServiceImpl implements CartService {

	/**
	 * Log events
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final CartDAO cartDAO;
	
	/**
	 * Initialise CartService.
	 * 
	 * @param cartDAOImpl
	 */
	@Inject
	public CartServiceImpl(CartDAO cartDAO) {
		this.cartDAO = cartDAO;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Cart> createCart(Cart cart) {
		logger.info("createCart(Cart cart) arg : {}", cart);
		return Observable.create(source -> {
			try {
				source.onNext(cartDAO.createAndFlush(cart));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Cart> updateCart(Cart cart) {
		logger.info("updateCart(Cart cart) arg : {}", cart);
		return Observable.create(source -> {
			try {
				source.onNext(cartDAO.updateAndFlush(cart));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Cart> readCart(Long id) {
		logger.info("readCart(Long id) arg : {}", id);
		return Observable.create(source -> {
			try {
				source.onNext(cartDAO.get(Cart.class, id));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Cart> listCart(Paging paging, Account account) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Cart> removeCart(Long id) {
		logger.info("removeCart(Long id) arg : {}", id);
		return Observable.<Cart>create(source ->  {
			try {
				Cart cart = cartDAO.get(Cart.class, id);
				cartDAO.delete(cart);
				source.onNext(cart);
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

}
