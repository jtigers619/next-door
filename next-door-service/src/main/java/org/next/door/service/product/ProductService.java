package org.next.door.service.product;

import javax.validation.constraints.NotNull;

import org.next.door.model.paging.Paging;
import org.next.door.model.product.Product;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
public interface ProductService {
	
	/**
	 * Create Product
	 * 
	 * @param product
	 * @return Observable<Product>
	 */
	Observable<Product> createProduct(Product product);
	
	/**
	 * Update Product
	 * 
	 * @param Product
	 * @return Observable<Product>
	 */
	Observable<Product> updateProduct(Product product);
	
	/**
	 * Read Product
	 * 
	 * @param id
	 * @return Observable<Product>
	 */
	Observable<Product> readProduct(Long id);
	
	/**
	 * Get list of {@link Product}
	 * 
	 * @param paging
	 * @return Observable<Product>
	 */
	Observable<Product> listProduct(@NotNull Paging paging);
	
	/**
	 * Remove Product
	 * 
	 * @param id
	 * @return Product
	 */
	Observable<Product> removeProduct(Long id);
}
