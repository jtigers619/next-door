package org.next.door.service.orderdetail;

import javax.inject.Inject;

import org.next.door.dao.orderdetail.OrderDetailDAO;
import org.next.door.model.account.Account;
import org.next.door.model.orderdetail.OrderDetail;
import org.next.door.model.paging.Paging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
@Service
public class OrderDetailServiceImpl implements OrderDetailService {
	/**
	 * Log events
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final OrderDetailDAO orderDetailDAO;
	
	/**
	 * Initialise OrderDetailService
	 * 
	 * @param orderDetailDAO
	 */
	
	@Inject
	public OrderDetailServiceImpl(OrderDetailDAO orderDetailDAO) {
		this.orderDetailDAO = orderDetailDAO;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<OrderDetail> createOrderDetail(OrderDetail orderDetail) {
		logger.info("createOrderDetail(OrderDetail orderDetail) arg : {}", orderDetail);
		return Observable.create(source -> {
			try {
				source.onNext(orderDetailDAO.createAndFlush(orderDetail));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<OrderDetail> updateOrderDetail(OrderDetail orderDetail) {
		logger.info("updateOrderDetail(OrderDetail orderDetail) arg : {}", orderDetail);
		return Observable.create(source -> {
			try {
				source.onNext(orderDetailDAO.updateAndFlush(orderDetail));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<OrderDetail> readOrderDetail(Long id) {
		logger.info("readOrderDetail(Long id) arg : {}", id);
		return Observable.create(source -> {
			try {
				source.onNext(orderDetailDAO.get(OrderDetail.class, id));
				source.onComplete();
			} catch (Exception e) {
				source.onError(e);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<OrderDetail> listOrderDetail(Paging paging, Account account) {
		logger.info("listOrderDetail(Paging paging, Account account) arg : {}, {}", paging, account);
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<OrderDetail> removeOrderDetail(Long id) {
		logger.info("removeOrderDetail(Long id) arg : {}", id);
		return Observable.create(source -> {
				try {
					OrderDetail orderDetail = orderDetailDAO.get(OrderDetail.class, id);
					orderDetailDAO.delete(orderDetail);
					source.onNext(orderDetail);
					source.onComplete();
				} catch (Exception e) {
					source.onError(e);
				}
			});
	}

}
