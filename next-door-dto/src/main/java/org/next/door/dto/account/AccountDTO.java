package org.next.door.dto.account;

import java.util.List;

import org.next.door.dto.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ad
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false, exclude = { "addresses" })
@ToString(exclude = "addresses")
@Builder
public class AccountDTO implements DTO {
	private Long accountId;
	private String mobileNo;
	private String email;
	private String fullName;
	private String password;
	private boolean active;
	private String userRole;
	private List<AddressDTO> addresses;
}
