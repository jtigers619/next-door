package org.next.door.dto.order;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.next.door.dto.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ad
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class OrderDTO implements DTO {
	private Long orderId;
	private LocalDate orderDate;
	private Long orderNumber;
	private BigDecimal amount;
}
