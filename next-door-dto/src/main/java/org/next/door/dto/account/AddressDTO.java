package org.next.door.dto.account;

import org.next.door.dto.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  An Address represents the address of an {@link AccountDTO}. There is no mean to create a saparate entity because an address never exist without an
 * {@link AccountDTO}.
 * 
 * @author ad
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddressDTO implements DTO {
	private Long addressId;
	private String address;
	private String city;
	private String country;
	private String zipCode;
}
