package org.next.door.dto.orderdetail;

import java.math.BigDecimal;

import org.next.door.dto.DTO;
import org.next.door.dto.order.OrderDTO;
import org.next.door.dto.product.ProductDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ad
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, exclude = { "order", "product" })
@ToString(exclude = { "order", "product" })
@Builder
public class OrderDetailDTO implements DTO { 
	private Long orderDetailId;
	private int quanity;
	private BigDecimal price;
	private BigDecimal amount;
	private OrderDTO order;
	private ProductDTO product;
}
