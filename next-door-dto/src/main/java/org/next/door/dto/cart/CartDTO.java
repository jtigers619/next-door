package org.next.door.dto.cart;

import java.math.BigDecimal;
import java.util.List;

import org.next.door.dto.DTO;
import org.next.door.dto.product.ProductDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ad
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false, exclude = { "products" })
@ToString(exclude = "products")
@Builder
public class CartDTO implements DTO {
	
	private Long cartId;
	private BigDecimal cartAmount;
	private List<ProductDTO> products;
}
