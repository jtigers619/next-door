package org.next.door.dto.product;

import java.math.BigDecimal;

import org.next.door.dto.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ad
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ToString
@Builder
public class ProductDTO implements DTO {
	private Long productId;
	private String code;
	private String name;
	private BigDecimal price;
	private byte[] image;
}
