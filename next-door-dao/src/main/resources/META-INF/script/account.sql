-- Table: account

-- DROP TABLE account;

--CREATE TABLE account
--(
  --id bigint NOT NULL,
  --created_on timestamp without time zone NOT NULL,
  --modified_on timestamp without time zone,
  --version bigint,
  --account_non_expired boolean,
  --account_non_locked boolean,
  --city character varying(255),
  --country character varying(255),
  --street character varying(255),
  --credential_non_expired boolean,
  --email_address character varying(255),
  --enabled boolean,
  --firstname character varying(255),
  --deleted boolean,
  --lastname character varying(255),
  --mobile_no character varying(255),
  --password character varying(255),
  --username character varying(255),
  --CONSTRAINT account_pkey PRIMARY KEY (id),
  --CONSTRAINT uk_taw3cgffo1ugdxecdmxplpv0f UNIQUE (username, email_address, mobile_no)
--)
--WITH (
--  OIDS=FALSE
--);
--ALTER TABLE account
  --OWNER TO lmsweb;
INSERT INTO account(
            id, created_on, version, account_non_expired, account_non_locked, 
            city, country, street, credential_non_expired, email_address, 
            enabled, firstname, deleted, lastname, mobile_no, password, username)
    VALUES (nextval('LMS_USER_ID_SEQ'), now(), 0, TRUE, TRUE, 'YNR', 
            'INDIA', '#18', TRUE, 'amit.dhiman@blinfosoft.com', TRUE, 
            'Amit', FALSE, 'Dhiman', '0721806751', '9ea3d39668368cb46562d6290a6bb48daea033617787b78a9334b5baa3dbd535', 'amit.dhiman');


--password is login@123

INSERT INTO role(id, name)
    VALUES (nextval('LMS_ROLE_ID_SEQ'), 'ADMIN');
INSERT INTO role(id, name)
    VALUES (nextval('LMS_ROLE_ID_SEQ'),'USER');
INSERT INTO account_role(
            account_id, roles_id)
    VALUES (1, 1);
            
INSERT INTO account_role(
            account_id, roles_id)
    VALUES (1, 2);
