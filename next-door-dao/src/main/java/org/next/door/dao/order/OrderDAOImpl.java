package org.next.door.dao.order;

import org.next.door.dao.base.BaseDAOImpl;
import org.next.door.model.order.Order;
import org.springframework.stereotype.Repository;

/**
 * @author ad
 *
 */
@Repository
public class OrderDAOImpl extends BaseDAOImpl<Order> implements OrderDAO {

}