package org.next.door.dao.order;

import org.next.door.dao.base.BaseDAO;
import org.next.door.model.order.Order;

/**
 * @author ad
 *
 */
public interface OrderDAO extends BaseDAO<Order>{

}
