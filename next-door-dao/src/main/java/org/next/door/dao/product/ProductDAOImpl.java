package org.next.door.dao.product;

import javax.persistence.criteria.CriteriaQuery;

import org.next.door.dao.base.BaseDAOImpl;
import org.next.door.model.paging.Paging;
import org.next.door.model.product.Product;
import org.springframework.stereotype.Repository;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
@Repository
public class ProductDAOImpl extends BaseDAOImpl<Product> implements ProductDAO {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Product> list(Paging paging) {
		CriteriaQuery<Product> criteriaQuery = entityManager.getCriteriaBuilder()
				.createQuery(Product.class);
		return Observable.<Product>fromIterable(
				entityManager
					.createQuery(criteriaQuery.select(criteriaQuery.from(Product.class)))
					.setFirstResult(paging.getStartIndex())
					.setMaxResults(paging.getPageSize())
					.getResultList()
					);
	}

}
