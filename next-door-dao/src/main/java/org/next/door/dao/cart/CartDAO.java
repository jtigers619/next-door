package org.next.door.dao.cart;

import org.next.door.dao.base.BaseDAO;
import org.next.door.model.cart.Cart;

/**
 * @author ad
 *
 */
public interface CartDAO extends BaseDAO<Cart> {

}
