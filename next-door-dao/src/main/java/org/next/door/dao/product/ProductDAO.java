package org.next.door.dao.product;

import org.next.door.dao.base.BaseDAO;
import org.next.door.model.paging.Paging;
import org.next.door.model.product.Product;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
public interface ProductDAO extends BaseDAO<Product>{
	
	/**
	 * Get list of {@link Product}
	 * 
	 * @param paging
	 * @return Observable<Product>
	 */
	Observable<Product> list(Paging paging);
}
