package org.next.door.dao.configuration;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author ad
 *
 */
@Configuration
@ComponentScan(basePackages = { "org.next.door.dao" })
@PropertySource(value = { "classpath:/META-INF/db/jdbc-hibernate.properties" })
@EnableTransactionManagement
public class DatabaseConfiguration {

	private static final String HIBERNATE_DIALECT = "hibernate.dialect";
	private static final String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
	private static final String HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";
	private static final String HIBERNATE_EJB_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";
	private static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
	private static final String HIBERNATE_SHOW_GENERATED_DDL = "hibernate.show.generated.ddl";
	private static final String PERSISTENCE_UNIT_NAME = "nextDoorModel";

	/**
	 * Environment has access to all environmental variable and properties
	 * values.
	 */
	@Autowired
	private Environment environment;

	/**
	 * Data source for connection with database.
	 * 
	 * @return {@link DataSource} - for DB connection.
	 */
	@Bean(destroyMethod = "")
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("database.driver"));
		dataSource.setUrl(environment.getRequiredProperty("database.url"));
		dataSource.setUsername(environment.getRequiredProperty("database.username"));
		dataSource.setPassword(environment.getRequiredProperty("database.password"));
		return dataSource;
	}

	/**
	 * JPA adapter for hibernate.
	 * 
	 * @return {@link HibernateJpaVendorAdapter} - return vendor adapter for JPA.
	 */
	private HibernateJpaVendorAdapter getHibernateJpaVendorAdaptor() {
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setGenerateDdl(Boolean.parseBoolean(environment.getProperty(HIBERNATE_SHOW_GENERATED_DDL)));
		adapter.setShowSql(Boolean.parseBoolean(environment.getProperty(HIBERNATE_SHOW_SQL)));
		adapter.setDatabasePlatform(environment.getProperty(HIBERNATE_DIALECT));
		adapter.setDatabase(Database.MYSQL);
		return adapter;
	}

	/**
	 * Use dataSource and hibernateJpaVendorAdapter to create entityManagerFactory.
	 * 
	 * @return {@link LocalContainerEntityManagerFactoryBean} - cunstructed entityManagerFactory.
	 * @throws IOException 
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean() throws IOException {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(dataSource());
		entityManagerFactoryBean.setJpaVendorAdapter(getHibernateJpaVendorAdaptor());
		entityManagerFactoryBean.setJpaProperties(jpaProperties());
		entityManagerFactoryBean.setPersistenceXmlLocation("classpath:META-INF/persistence.xml");
		entityManagerFactoryBean.setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
		entityManagerFactoryBean.setPackagesToScan("org.next.door.model");
		entityManagerFactoryBean.afterPropertiesSet();

		return entityManagerFactoryBean;
	}

	/**
	 * Create and return {@link JpaTransactionManager} for transactions.
	 * 
	 * @param entityManagerFactoryBean - LocalContainerEntityManagerFactoryBean
	 * @return {@link PlatformTransactionManager} - transaction manager to use while database oprations.
	 */
	@Autowired
	@Bean
	public PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManagerFactoryBean) {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactoryBean.getObject());
		jpaTransactionManager.setValidateExistingTransaction(Boolean.TRUE);
		jpaTransactionManager.setDefaultTimeout(TransactionDefinition.TIMEOUT_DEFAULT);
		return jpaTransactionManager;
	}

	/**
	 * Use {@link PersistenceExceptionTranslationPostProcessor} for exceptions.
	 * 
	 * @return {@link PersistenceExceptionTranslationPostProcessor}.
	 */
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	/**
	 * Hibernate properties to use.
	 * 
	 * @return {@link Properties} - to use.
	 */
	private Properties jpaProperties() {
		Properties props = new Properties();
		props.setProperty(HIBERNATE_DIALECT, environment.getProperty(HIBERNATE_DIALECT));
		props.setProperty(HIBERNATE_FORMAT_SQL, environment.getProperty(HIBERNATE_FORMAT_SQL));
		props.setProperty(HIBERNATE_HBM2DDL_AUTO, environment.getProperty(HIBERNATE_HBM2DDL_AUTO));
		props.setProperty(HIBERNATE_EJB_NAMING_STRATEGY, environment.getProperty(HIBERNATE_EJB_NAMING_STRATEGY));
		props.setProperty(HIBERNATE_SHOW_SQL, environment.getProperty(HIBERNATE_SHOW_SQL));
		// add more
		return props;
	}

}
