package org.next.door.dao.account;

import org.next.door.dao.base.BaseDAO;
import org.next.door.model.account.Account;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
public interface AccountDAO extends BaseDAO<Account>{
	
	/**
	 * JPQL named query to get {@link Account} by email.
	 */
	String READ_ACCOUNT_BY_EMAIL = "read.account.email";
	
	/**
	 * JPQL named query to get all {@link Account}.
	 */
	String LIST_ACCOUNTS = "list.accounts";
	
	/**
	 * Email field of {@link Account}.
	 */
	String EMAIL = "email";
	
	/**
	 * Read an {@link Account} from DB.
	 * 
	 * @param email
	 * @return Observable<Account>
	 */
	Observable<Account> readByEmail(String email);
	
	/**
	 * List all {@link Account} from DB.
	 * 
	 * @return Observable<Account>
	 */
	Observable<Account> list ();
}
