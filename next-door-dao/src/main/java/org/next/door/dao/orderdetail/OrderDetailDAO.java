package org.next.door.dao.orderdetail;

import org.next.door.dao.base.BaseDAO;
import org.next.door.model.orderdetail.OrderDetail;

/**
 * @author ad
 *
 */
public interface OrderDetailDAO extends BaseDAO<OrderDetail> {

}
