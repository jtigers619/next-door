package org.next.door.dao.orderdetail;

import org.next.door.dao.base.BaseDAOImpl;
import org.next.door.model.orderdetail.OrderDetail;
import org.springframework.stereotype.Repository;

/**
 * @author ad
 *
 */
@Repository
public class OrderDetailDAOImpl extends BaseDAOImpl<OrderDetail> implements OrderDetailDAO {

}