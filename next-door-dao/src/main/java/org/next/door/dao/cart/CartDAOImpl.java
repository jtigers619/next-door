package org.next.door.dao.cart;

import org.next.door.dao.base.BaseDAOImpl;
import org.next.door.model.cart.Cart;
import org.springframework.stereotype.Repository;

/**
 * @author ad
 *
 */
@Repository
public class CartDAOImpl extends BaseDAOImpl<Cart> implements CartDAO {

}
