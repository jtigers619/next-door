package org.next.door.dao.account;

import org.next.door.dao.base.BaseDAOImpl;
import org.next.door.model.account.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import io.reactivex.Observable;

/**
 * @author ad
 *
 */
@Repository
public class AccountDAOImpl extends BaseDAOImpl<Account> implements AccountDAO {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Account> readByEmail(String email) {
		logger.info("Email id is : {}.", email);
		return Observable.<String>just(email).
				map(emailAdd -> entityManager
					.createNamedQuery(READ_ACCOUNT_BY_EMAIL, Account.class)
					.setParameter(EMAIL, emailAdd)
					.getSingleResult());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Observable<Account> list() {
		return Observable.<Account>fromIterable(entityManager.createNamedQuery(LIST_ACCOUNTS, Account.class).getResultList());
	}
	
	
}
