# Gradle 3.5 | SpringBoot 1.5x | Spring 4.3.x | Jersey 2.x | Hibernate 5x | RXJava 2.x#

This is a sample application.

## What is this repository for? ##

* Quick summary
	A simple application demonstrate manly gradle spring boot jersey with rxjava async response.
* Version
	:)

## What you’ll need ##

* Eclipse, NetBeans, Idea IDE
* JDK 1.8 or later
* Gradle 3.5+ (Install gradle plugin in eclipse)
* git clone 

### Summary of set up ###

* Import this project in your favorite IDE.
* Run command gradlew clean build 
* Start spring boot gradlew bootRun	

### Database configuration ###

* Find every thing you need in next-door-dao module

### How to run tests ###
* No test yet till another dev day :)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact