package org.next.door.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Version;

import org.apache.commons.lang3.SerializationUtils;
import org.next.door.model.listener.AuditEntityListener;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Base class to derive entity classes from
 * 
 * @author ad
 *
 */
@Data
@EqualsAndHashCode
@EntityListeners(AuditEntityListener.class)
public class AbstractEntity implements Serializable, EntityContract {

	/**
	 * Serial version UUID.
	 */
	private static final long serialVersionUID = -4288056230058234586L;
	
	 	@Column(name = "created_on", nullable = false)
	    private LocalDateTime createdOn;

	    @Column(name = "modified_on")
	    private LocalDateTime modifiedOn;

	    @Version
	    private Long version;

	    /**
	     * Serialization based clone. <strong>Might be slow - so take care</strong> {@inheritDoc}
	     */
	    @Override
	    public Object clone() throws CloneNotSupportedException {
	        return SerializationUtils.clone(this);
	    }

}
