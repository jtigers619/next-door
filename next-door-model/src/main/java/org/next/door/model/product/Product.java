package org.next.door.model.product;

import java.math.BigDecimal;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.next.door.model.AbstractEntity;
import org.next.door.model.cart.Cart;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ad
 *
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, exclude = { "cart" })
@ToString(exclude = "cart")
@Builder
@Entity
@Table(name = "products")
public class Product extends AbstractEntity {

	/**
	 * Serial version UUID.
	 */
	private static final long serialVersionUID = -1350701772533419689L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "product_id", length = 50)
	private Long productId;
	
	@Column(name = "code", length = 20, nullable = false)
	private String code;

	@Column(name = "name", length = 255, nullable = false)
	private String name;

	@Column(name = "price", nullable = false)
	private BigDecimal price;
	
	public Optional<BigDecimal> getPrice() {
		return Optional.ofNullable(price);
	}

	@Lob
	@Column(name = "image", length = Integer.MAX_VALUE, nullable = true)
	private byte[] image;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_cart_id", nullable = false, foreignKey = @ForeignKey(name = "products_cart_fk"))
	private Cart cart;

}
