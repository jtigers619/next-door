package org.next.door.model.orderdetail;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.next.door.model.AbstractEntity;
import org.next.door.model.order.Order;
import org.next.door.model.product.Product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ad
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, exclude = { "order", "product" })
@ToString(exclude = { "order", "product" })
@Builder
@Entity
@Table(name = "order_details")
public class OrderDetail extends AbstractEntity {

	/**
	 * Serial version UUID.
	 */
	private static final long serialVersionUID = 5032306688793404468L;

	@Id
	@Column(name = "order_detail_id", length = 50, nullable = false)
	private Long orderDetailId;

	@Column(name = "quanity", nullable = false)
	private int quanity;

	@Column(name = "price", nullable = false)
	private BigDecimal price;

	@Column(name = "amount", nullable = false)
	private BigDecimal amount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_order_id", nullable = false, foreignKey = @ForeignKey(name = "order_details_orders_fk"))
	private Order order;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_product_id", nullable = false, foreignKey = @ForeignKey(name = "order_details_products_fk"))
	private Product product;
}
