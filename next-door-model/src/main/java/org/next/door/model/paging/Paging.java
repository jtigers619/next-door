package org.next.door.model.paging;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @author ad
 *
 */
public class Paging {
	
	@Min(0)
    private final int startIndex;

	@Max(500)
    private int pageSize;

    public Paging(int startIndex, int pageSize) {
        this.startIndex = startIndex;
        this.pageSize = pageSize;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getPageSize() {
    	if (pageSize==0) pageSize = 500;
        return pageSize;
    }
}
