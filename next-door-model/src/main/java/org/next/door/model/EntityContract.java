package org.next.door.model;

/**
 * Marker interface used in base dao to perform generic oprations on entity.
 * 
 * @author ad
 *
 */
public interface EntityContract {

}
