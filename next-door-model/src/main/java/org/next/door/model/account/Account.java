package org.next.door.model.account;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.next.door.model.AbstractEntity;
import org.next.door.model.order.Order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * http://o7planning.org/en/10605/create-a-java-shopping-cart-web-application-using-spring-mvc-and-hibernate#a2563862
 * 
 * @author ad
 *
 */
@Entity
@Table(name = "accounts")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, exclude = { "addresses", "orders" })
@ToString(exclude = { "addresses", "orders" })
@Builder
public class Account extends AbstractEntity {

	/**
	 * Serial version UUID.
	 */
	private static final long serialVersionUID = 6909465143980208596L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "account_id")
	private Long accountId;

	@Column(name = "mobile_no", length = 13, nullable = true)
	private String mobileNo;

	@Column(name = "email", length = 30, nullable = true)
	private String email;

	@Column(name = "full_name", length = 30, nullable = true)
	private String fullName;

	@Column(name = "password", length = 30, nullable = true)
	private String password;

	@Column(name = "active", nullable = false, columnDefinition = "tinyint(1) default '0'")
	private boolean active;

	@Column(name = "user_role", length = 30, nullable = false)
	private String userRole;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "account")
	private List<Address> addresses;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "account")
	private List<Order> orders;
}
