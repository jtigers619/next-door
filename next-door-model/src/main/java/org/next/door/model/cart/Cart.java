package org.next.door.model.cart;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.next.door.commons.collectors.NextDoorCollectors;
import org.next.door.model.AbstractEntity;
import org.next.door.model.account.Account;
import org.next.door.model.product.Product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ad
 *
 */
@Entity
@Table(name = "cart")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, exclude = { "account", "products" })
@ToString(exclude = { "account", "products" })
@Builder
public class Cart extends AbstractEntity {

	/**
	 * Serial version UUID
	 */
	private static final long serialVersionUID = 8488973357016620146L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cart_id", length = 50)
	private Long cartId;

	@Column(name = "amount", nullable = false)
	private BigDecimal cartAmount;

	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "fk_account_id", nullable = false, foreignKey = @ForeignKey(name = "carts_accounts_fk"))
	private Account account;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cart")
	private List<Product> products;
	
	public Optional<BigDecimal> getCartAmount () {
		Stream<Product> productStream = products != null && products.size() > 0 ? products.stream() : Stream.<Product>empty(); 
		return productStream
				.map(product -> product.getPrice())
				.collect(NextDoorCollectors.sum());
	}
}
