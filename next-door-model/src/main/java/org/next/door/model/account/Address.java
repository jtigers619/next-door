package org.next.door.model.account;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  An Address represents the address of an {@link Account}. There is no mean to create a saparate entity because an address never exist without an
 * {@link Account}.
 * 
 * @author ad
 *
 */
@Entity
@Table(name = "address")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Address implements Serializable {

	/**
	 * Serial version UUID.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "address_id")
	private Long addressId;

	@Column(name = "address", length = 100, nullable = false)
	private String address;

	@Column(name = "city", length = 80, nullable = false)
	private String city;

	@Column(name = "country", length = 80, nullable = false)
	private String country;

	@Column(name = "zip_code", length = 12, nullable = false)
	private String zipCode;

	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "fk_account_id", nullable = false, foreignKey = @ForeignKey(name = "address_accounts_fk"))
	private Account account;
}
