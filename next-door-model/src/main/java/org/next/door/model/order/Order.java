package org.next.door.model.order;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.next.door.model.AbstractEntity;
import org.next.door.model.account.Account;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ad
 *
 */
@Entity
@Table(name = "orders", uniqueConstraints = { @UniqueConstraint(columnNames = "order_number") })
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, exclude = { "account" })
@ToString(exclude = "account")
@Builder
public class Order extends AbstractEntity {

	/**
	 * Serial version UUID
	 */
	private static final long serialVersionUID = -5724314404477368948L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "order_id", length = 50)
	private Long orderId;

	@Column(name = "order_date", nullable = false)
	private LocalDate orderDate;

	@Column(name = "order_number", nullable = false)
	private Long orderNumber;

	@Column(name = "amount", nullable = false)
	private BigDecimal amount;

	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "fk_account_id", nullable = false, foreignKey = @ForeignKey(name = "orders_accounts_fk"))
	private Account account;

}