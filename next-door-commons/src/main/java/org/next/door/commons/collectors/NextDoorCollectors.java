package org.next.door.commons.collectors;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;

/**
 * @author ad
 *
 */
public interface NextDoorCollectors {
	
	public static <T> Collector<Optional<BigDecimal>, List<BigDecimal>, Optional<BigDecimal>> sum() {
        return Collector.of(
        		() -> new ArrayList<>(), 
        		(acc, value) -> value.ifPresent(acc::add), 
        		(l1,l2) -> {
        			l1.addAll(l2);
        			return l1;
        		}, 
        		acc -> acc.isEmpty() ? Optional.empty() : Optional.of(acc.stream().reduce(BigDecimal.ZERO, BigDecimal::add))
        	);
    }
}
