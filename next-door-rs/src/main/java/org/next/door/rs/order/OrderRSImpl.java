package org.next.door.rs.order;

import static org.next.door.rs.response.ResponseHandler.ASYNC_ERROR_RESPONSE;
import static org.next.door.rs.response.ResponseHandler.ASYNC_RESPONSE;

import javax.ws.rs.container.AsyncResponse;

import org.next.door.converter.order.OrderConverter;
import org.next.door.dto.order.OrderDTO;
import org.next.door.model.paging.Paging;
import org.next.door.service.order.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ad
 *
 */
public class OrderRSImpl implements OrderRS {

	/**
	 * Log events
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final OrderService orderService;
	
	public OrderRSImpl (OrderService orderService) {
		this.orderService = orderService;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createOrder(AsyncResponse asyncResponse, OrderDTO orderDTO) {
		orderService.createOrder(OrderConverter.fromOrderDTOToOrder(orderDTO))
			.map(order -> OrderConverter.fromOrderToOrderDTO(order))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("Order created")
				);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateOrder(AsyncResponse asyncResponse, Long id, OrderDTO orderDTO) {
		orderDTO.setOrderId(id);
		orderService.updateOrder(OrderConverter.fromOrderDTOToOrder(orderDTO))
			.map(order -> OrderConverter.fromOrderToOrderDTO(order))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("Order updated with id {}.", id)
				);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteOrder(Long id, AsyncResponse asyncResponse) {
		orderService.removeOrder(id)
			.map(order -> OrderConverter.fromOrderToOrderDTO(order))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("Order removed with id {}.", id)
				);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void readOrder(AsyncResponse asyncResponse, Long id) {
		orderService.readOrder(id)
			.map(order -> OrderConverter.fromOrderToOrderDTO(order))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("Order read with id {}.", id)
				);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void listOrder(AsyncResponse asyncResponse, Paging paging) {
		// TODO Auto-generated method stub

	}

}
