package org.next.door.rs.orderdetail;

import static org.next.door.rs.response.ResponseHandler.ASYNC_ERROR_RESPONSE;
import static org.next.door.rs.response.ResponseHandler.ASYNC_RESPONSE;

import javax.ws.rs.container.AsyncResponse;

import org.next.door.converter.orderdetail.OrderDetailConverter;
import org.next.door.dto.orderdetail.OrderDetailDTO;
import org.next.door.model.paging.Paging;
import org.next.door.service.orderdetail.OrderDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ad
 *
 */
public class OrderDetailRSImpl implements OrderDetailRS {

	/**
	 * Log events
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final OrderDetailService orderDetailService;
	
	public OrderDetailRSImpl (OrderDetailService orderDetailService) {
		this.orderDetailService = orderDetailService;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createOrderDetail(final AsyncResponse asyncResponse, OrderDetailDTO orderDetailDTO) {
		orderDetailService.createOrderDetail(OrderDetailConverter.fromOrderDetailDTOToOrder(orderDetailDTO))
			.map(orderDetail -> OrderDetailConverter.fromOrderDetailToOrderDTO(orderDetail))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("OrderDetail created")
				);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateOrderDetail(final AsyncResponse asyncResponse, Long id, OrderDetailDTO orderDetailDTO) {
		orderDetailDTO.setOrderDetailId(id);
		orderDetailService.updateOrderDetail(OrderDetailConverter.fromOrderDetailDTOToOrder(orderDetailDTO))
			.map(orderDetail -> OrderDetailConverter.fromOrderDetailToOrderDTO(orderDetail))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("OrderDetail updated with id {}.", id)
				);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteOrderDetail(Long id, final AsyncResponse asyncResponse) {
		orderDetailService.removeOrderDetail(id)
			.map(orderDetail -> OrderDetailConverter.fromOrderDetailToOrderDTO(orderDetail))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("Remove orderdetails with id {}.", id)
				);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void readOrderDetail(AsyncResponse asyncResponse, Long id) {
		orderDetailService.readOrderDetail(id)
			.map(orderDetail -> OrderDetailConverter.fromOrderDetailToOrderDTO(orderDetail))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("Read orderdetails with id {}.", id)
				);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void listOrderDetail(AsyncResponse asyncResponse, Paging paging) {
		// TODO Auto-generated method stub

	}

}
