package org.next.door.rs.cart;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

import org.next.door.dto.cart.CartDTO;
import org.next.door.model.paging.Paging;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ad
 *
 */
@Path("/cart")
@Produces(value = {MediaType.APPLICATION_JSON})
@Consumes(value = {MediaType.APPLICATION_JSON})
@Transactional
public interface CartRS {
	
	/**
	 * Rest create Cart.
	 */
	@POST
	public void createCart(@Suspended final AsyncResponse asyncResponse, CartDTO cartDTO);
	
	/**
	 * Rest update Cart.
	 */
	@Path("/{id}")
	@PUT
	public void updateCart(@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long id, CartDTO cartDTO);
	
	/**
	 * Rest delete Cart.
	 */
	@Path("/{id}")
	@DELETE
	public void deleteCart(@PathParam("id") Long id, @Suspended final AsyncResponse asyncResponse);
	
	/**
	 * Rest read Cart by id.
	 */
	@Path("/{id}")
	@GET
	@Transactional(readOnly = true)
	public void readCart(@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long id);
	
	/**
	 * Rest list Cart.
	 */
	@Path("/list")
	@POST
	@Transactional(readOnly = true)
	public void listCart(@Suspended final AsyncResponse asyncResponse, Paging paging);
}
