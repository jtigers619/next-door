package org.next.door.rs.orderdetail;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

import org.next.door.dto.orderdetail.OrderDetailDTO;
import org.next.door.model.paging.Paging;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ad
 *
 */
@Path("/orderdetails")
@Produces(value = {MediaType.APPLICATION_JSON})
@Consumes(value = {MediaType.APPLICATION_JSON})
@Transactional
public interface OrderDetailRS {

	/**
	 * Rest create OrderDetail.
	 */
	@POST
	public void createOrderDetail(@Suspended final AsyncResponse asyncResponse, OrderDetailDTO orderDetailDTO);
	
	/**
	 * Rest update OrderDetail.
	 */
	@Path("/{id}")
	@PUT
	public void updateOrderDetail(@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long id, OrderDetailDTO orderDetailDTO);
	
	/**
	 * Rest delete OrderDetail.
	 */
	@Path("/{id}")
	@DELETE
	public void deleteOrderDetail(@PathParam("id") Long id, @Suspended final AsyncResponse asyncResponse);
	
	/**
	 * Rest read OrderDetail by id.
	 */
	@Path("/{id}")
	@GET
	@Transactional(readOnly = true)
	public void readOrderDetail(@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long id);
	
	/**
	 * Rest list OrderDetail.
	 */
	@Path("/list")
	@POST
	@Transactional(readOnly = true)
	public void listOrderDetail(@Suspended final AsyncResponse asyncResponse, Paging paging);
}
