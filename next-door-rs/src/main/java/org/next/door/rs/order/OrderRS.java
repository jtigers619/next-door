package org.next.door.rs.order;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

import org.next.door.dto.order.OrderDTO;
import org.next.door.model.paging.Paging;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ad
 *
 */
@Path("/orders")
@Produces(value = {MediaType.APPLICATION_JSON})
@Consumes(value = {MediaType.APPLICATION_JSON})
@Transactional
public interface OrderRS {
	
	/**
	 * Rest create Order.
	 */
	@POST
	public void createOrder(@Suspended final AsyncResponse asyncResponse, OrderDTO orderDTO);
	
	/**
	 * Rest update Order.
	 */
	@Path("/{id}")
	@PUT
	public void updateOrder(@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long id, OrderDTO orderDTO);
	
	/**
	 * Rest delete Order.
	 */
	@Path("/{id}")
	@DELETE
	public void deleteOrder(@PathParam("id") Long id, @Suspended final AsyncResponse asyncResponse);
	
	/**
	 * Rest read Order by id.
	 */
	@Path("/{id}")
	@GET
	@Transactional(readOnly = true)
	public void readOrder(@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long id);
	
	/**
	 * Rest list Order.
	 */
	@Path("/list")
	@POST
	@Transactional(readOnly = true)
	public void listOrder(@Suspended final AsyncResponse asyncResponse, Paging paging);
}
