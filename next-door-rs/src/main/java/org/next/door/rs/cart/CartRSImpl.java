package org.next.door.rs.cart;

import static org.next.door.rs.response.ResponseHandler.ASYNC_ERROR_RESPONSE;
import static org.next.door.rs.response.ResponseHandler.ASYNC_RESPONSE;

import javax.ws.rs.container.AsyncResponse;

import org.next.door.converter.cart.CartConverter;
import org.next.door.dto.cart.CartDTO;
import org.next.door.model.paging.Paging;
import org.next.door.service.cart.CartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author ad
 *
 */
@Component
public class CartRSImpl implements CartRS {

	/**
	 * Log events
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final CartService cartService;
	
	public CartRSImpl(CartService cartService) {
		this.cartService = cartService;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createCart(final AsyncResponse asyncResponse, CartDTO cartDTO) {
		cartService.createCart(CartConverter.fromCartDTOToCart(cartDTO))
			.map(cart -> CartConverter.fromCartToCartDTO(cart))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("Cart created")
				);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateCart(final AsyncResponse asyncResponse, Long id, CartDTO cartDTO) {
		cartDTO.setCartId(id);
		cartService.updateCart(CartConverter.fromCartDTOToCart(cartDTO))
			.map(cart -> CartConverter.fromCartToCartDTO(cart))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("Cart updated with id {}.", id)
				);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteCart(Long id, final AsyncResponse asyncResponse) {
		cartService.removeCart(id)
			.map(cart -> CartConverter.fromCartToCartDTO(cart))
			.subscribe(
				ASYNC_RESPONSE.apply(asyncResponse), 
				ASYNC_ERROR_RESPONSE.apply(asyncResponse),
				() -> logger.info("Cart removed with id {}.", id)
				);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void readCart(final AsyncResponse asyncResponse, Long id) {
		cartService.removeCart(id)
		.map(cart -> CartConverter.fromCartToCartDTO(cart))
		.subscribe(
			ASYNC_RESPONSE.apply(asyncResponse), 
			ASYNC_ERROR_RESPONSE.apply(asyncResponse),
			() -> logger.info("Cart read with id {}.", id)
			);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void listCart(final AsyncResponse asyncResponse, Paging paging) {
		// TODO Auto-generated method stub

	}

}
