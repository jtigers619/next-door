package org.next.door.rs.product;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

import org.next.door.dto.product.ProductDTO;
import org.next.door.model.paging.Paging;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ad
 *
 */
@Path("/products")
@Produces(value = {MediaType.APPLICATION_JSON})
@Consumes(value = {MediaType.APPLICATION_JSON})
@Transactional
public interface ProductRS {
	
	/**
	 * Rest create Product.
	 */
	@POST
	public void createProduct(@Suspended final AsyncResponse asyncResponse, ProductDTO acountDTO);
	
	/**
	 * Rest update Product.
	 */
	@Path("/{id}")
	@PUT
	public void updateProduct(@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long id, ProductDTO acountDTO);
	
	/**
	 * Rest delete Product.
	 */
	@Path("/{id}")
	@DELETE
	public void deleteProduct(@PathParam("id") Long id, @Suspended final AsyncResponse asyncResponse);
	
	/**
	 * Rest read Product by id.
	 */
	@Path("/{id}")
	@GET
	@Transactional(readOnly = true)
	public void readProduct(@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long id);
	
	/**
	 * Rest list Product.
	 */
	@Path("/list")
	@POST
	@Transactional(readOnly = true)
	public void listProduct(@Suspended final AsyncResponse asyncResponse, Paging paging);
}
