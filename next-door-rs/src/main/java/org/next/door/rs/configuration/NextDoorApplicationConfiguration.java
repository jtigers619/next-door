/**
 * 
 */
package org.next.door.rs.configuration;

import org.next.door.service.configuration.ServiceConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author ad
 *
 */
@Configuration
@EnableAsync
@Import(ServiceConfiguration.class)
@ComponentScan(basePackages = { "org.next.door.rs" }, excludeFilters = @Filter(type = FilterType.REGEX, pattern = "org.next.door.rs.configuration"))
public class NextDoorApplicationConfiguration {

}
