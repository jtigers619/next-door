package org.next.door.rs.response;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.next.door.dto.DTO;

import io.reactivex.functions.Consumer;

/**
 * @author ad
 *
 */
public interface ResponseHandler {

	Function<AtomicReference<Response>, Consumer<Throwable>> ATOMIC_ERROR_RESPONSE = responseContainer -> {
		return error -> responseContainer
				.set(Response.status(500).entity("An error occurred retrieving all entities: " + error.getMessage())
						.type(MediaType.TEXT_PLAIN_TYPE).build());
	};

	Function<AtomicReference<Response>, Consumer<? super DTO>>  ATOMIC_RESPONSE = responseContainer -> {
		return dto -> responseContainer.set(Response.ok(dto).build());
	};

	Function<AsyncResponse, Consumer<Throwable>> ASYNC_ERROR_RESPONSE = response -> {
		return errorResponse -> response.resume(
				Response.status(500).entity("An error occurred retrieving all entities: " + errorResponse.getMessage())
						.type(MediaType.TEXT_PLAIN_TYPE).build());
	};

	Function<AsyncResponse, Consumer<? super List<? super DTO>>> ASYNC_LIST_RESPONSE = response -> {
		return dto -> response.resume(Response.ok(dto).build());
	};
	
	Function<AsyncResponse, Consumer<? super DTO>> ASYNC_RESPONSE = response -> {
		return dto -> response.resume(Response.ok(dto).build());
	};
}
