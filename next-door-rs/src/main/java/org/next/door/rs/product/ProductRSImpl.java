package org.next.door.rs.product;

import static org.next.door.rs.response.ResponseHandler.ASYNC_ERROR_RESPONSE;
import static org.next.door.rs.response.ResponseHandler.ASYNC_LIST_RESPONSE;
import static org.next.door.rs.response.ResponseHandler.ASYNC_RESPONSE;

import java.util.List;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

import org.next.door.converter.product.ProductConverter;
import org.next.door.dto.product.ProductDTO;
import org.next.door.model.paging.Paging;
import org.next.door.service.product.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import io.reactivex.functions.Consumer;

/**
 * @author ad
 *
 */
@Component
public class ProductRSImpl implements ProductRS {

	/**
	 * Log events
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final ProductService productService;
	
	/**
	 * Initialize ProductRSImpl
	 * 
	 * @param productService
	 */
	public ProductRSImpl (ProductService productService) {
		this.productService = productService;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createProduct(@Suspended final AsyncResponse asyncResponse, ProductDTO productDTO) {
		productService.createProduct(ProductConverter.fromProductDTOToProduct(productDTO))
			.map(product -> ProductConverter.fromProductToProductDTO(product))
			.subscribe(
					ASYNC_RESPONSE.apply(asyncResponse), 
					ASYNC_ERROR_RESPONSE.apply(asyncResponse), 
					() -> logger.info("Product created")
					);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateProduct(@Suspended final AsyncResponse asyncResponse, Long id, ProductDTO productDTO) {
		productDTO.setProductId(id);
		productService.updateProduct(ProductConverter.fromProductDTOToProduct(productDTO))
			.map(product -> ProductConverter.fromProductToProductDTO(product))
			.subscribe(
					ASYNC_RESPONSE.apply(asyncResponse), 
					ASYNC_ERROR_RESPONSE.apply(asyncResponse), 
					() -> logger.info("Product Updated with id : {}", id)
					);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteProduct(Long id, @Suspended final AsyncResponse asyncResponse) {
		productService.removeProduct(id)
			.map(product -> ProductConverter.fromProductToProductDTO(product))
			.subscribe(
					ASYNC_RESPONSE.apply(asyncResponse), 
					ASYNC_ERROR_RESPONSE.apply(asyncResponse), 
					() -> logger.info("Product Removed with id : {}", id)
					);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void readProduct(@Suspended final AsyncResponse asyncResponse, Long id) {
		productService.readProduct(id)
			.map(product -> ProductConverter.fromProductToProductDTO(product))
			.subscribe(
					ASYNC_RESPONSE.apply(asyncResponse), 
					ASYNC_ERROR_RESPONSE.apply(asyncResponse), 
					() -> logger.info("Product Read with id : {}", id)
					);
	}
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void listProduct(@Suspended final AsyncResponse asyncResponse, Paging paging) {
		productService.listProduct(paging)
			.map(product -> ProductConverter.fromProductToProductDTO(product))
			.toList()
			.subscribe((Consumer<? super List<ProductDTO>>) ASYNC_LIST_RESPONSE.apply(asyncResponse), 
					ASYNC_ERROR_RESPONSE.apply(asyncResponse)
					);
	}

}
