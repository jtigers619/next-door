package org.next.door.rs.boot;

import org.next.door.rs.configuration.NextDoorApplicationConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * @author ad
 *
 */
@SpringBootApplication
public class NextDoorBoot extends SpringBootServletInitializer {
	
	public static void main(String[] args) {
		new NextDoorBoot()
		.configure(new SpringApplicationBuilder(NextDoorApplicationConfiguration.class))
		.run(args);
    }
}
