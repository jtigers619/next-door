/**
 * 
 */
package org.next.door.rs.pingpong;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

/**
 * @author ad
 *
 */
@Component
@Path("/ping")
@Produces(value = {MediaType.APPLICATION_JSON})
@Consumes(value = {MediaType.APPLICATION_JSON})
public class PingPongRS {

	/**
	 * {@inheritDoc}
	 */
	@GET
	public String pingPong() {
		return "Ping Pong";
	}

}
