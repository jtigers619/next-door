package org.next.door.rs.account;

import static org.next.door.converter.account.AcountConverter.fromAcountDTOToAcount;
import static org.next.door.converter.account.AcountConverter.fromAcountToAcountDTO;
import static org.next.door.rs.response.ResponseHandler.ASYNC_ERROR_RESPONSE;
import static org.next.door.rs.response.ResponseHandler.ASYNC_LIST_RESPONSE;
import static org.next.door.rs.response.ResponseHandler.ASYNC_RESPONSE;
import static org.next.door.rs.response.ResponseHandler.ATOMIC_ERROR_RESPONSE;
import static org.next.door.rs.response.ResponseHandler.ATOMIC_RESPONSE;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;

import org.next.door.dto.account.AccountDTO;
import org.next.door.service.account.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import io.reactivex.functions.Consumer;

/**
 * @author ad
 *
 */
@Component
public class AccountRSImpl implements AccountRS {
	
	/**
	 * Log events
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final AccountService accountService;
	
	@Inject
	public AccountRSImpl (AccountService accountService) {
		this.accountService = accountService;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createAccount(@Suspended final AsyncResponse asyncResponse, AccountDTO acountDTO) {
		accountService.createAccount(fromAcountDTOToAcount(acountDTO))
			.map(account -> fromAcountToAcountDTO(account))
			.subscribe(ASYNC_RESPONSE.apply(asyncResponse), 
					ASYNC_ERROR_RESPONSE.apply(asyncResponse), 
					() -> logger.info("Account resource created"));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Response updateAccount(Long id, AccountDTO acountDTO) {
		final AtomicReference<Response> responseContainer = new AtomicReference<>();
		acountDTO.setAccountId(id);
		accountService.updateAccount(fromAcountDTOToAcount(acountDTO))
			.map(account -> fromAcountToAcountDTO(account))
			.subscribe(
					ATOMIC_RESPONSE.apply(responseContainer), 
					ATOMIC_ERROR_RESPONSE.apply(responseContainer)
					);
		return responseContainer.get();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteAccount(Long id, @Suspended final AsyncResponse asyncResponse) {
		accountService.removeAccount(id)
			.map(account -> fromAcountToAcountDTO(account))
			.subscribe(
					ASYNC_RESPONSE.apply(asyncResponse), 
					ASYNC_ERROR_RESPONSE.apply(asyncResponse), 
					() -> logger.info("Account resource deleted with id : {}", id)
					);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Response readAccount(@Suspended final AsyncResponse asyncResponse, String email) {
		final AtomicReference<Response> responseContainer = new AtomicReference<>();
		accountService.readAccount(email)
			.map(account -> fromAcountToAcountDTO(account))
			.subscribe(ASYNC_RESPONSE.apply(asyncResponse), 
					ASYNC_ERROR_RESPONSE.apply(asyncResponse)
			);
		return responseContainer.get();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void listAccount(@Suspended final AsyncResponse asyncResponse) {
		accountService.listAccount()
			.map(account -> fromAcountToAcountDTO(account))
			.toList()
			.subscribe((Consumer<? super List<AccountDTO>>) ASYNC_LIST_RESPONSE.apply(asyncResponse), 
					ASYNC_ERROR_RESPONSE.apply(asyncResponse));
	}
}
