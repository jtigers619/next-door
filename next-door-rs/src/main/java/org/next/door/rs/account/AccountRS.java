package org.next.door.rs.account;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.next.door.dto.account.AccountDTO;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ad
 *
 */
@Path("/accounts")
@Produces(value = {MediaType.APPLICATION_JSON})
@Consumes(value = {MediaType.APPLICATION_JSON})
@Transactional
public interface AccountRS {
	
	/**
	 * Rest create account.
	 */
	@POST
	void createAccount(@Suspended final AsyncResponse asyncResponse, AccountDTO acountDTO);
	
	/**
	 * Rest update account.
	 */
	@Path("/{id}")
	@PUT
	Response updateAccount(@PathParam("id") Long id, AccountDTO acountDTO);
	
	/**
	 * Rest delete account.
	 */
	@Path("/{id}")
	@DELETE
	void deleteAccount(@PathParam("id") Long id, @Suspended final AsyncResponse asyncResponse);
	
	/**
	 * Rest read account by email.
	 */
	@Path("/{email}")
	@GET
	@Transactional(readOnly = true)
	Response readAccount(@Suspended final AsyncResponse asyncResponse, @PathParam("email") String email);
	
	/**
	 * Rest list account.
	 */
	@GET
	@Transactional(readOnly = true)
	void listAccount(@Suspended final AsyncResponse asyncResponse);
}
