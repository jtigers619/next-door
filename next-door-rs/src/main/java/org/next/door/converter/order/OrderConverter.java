package org.next.door.converter.order;

import org.next.door.dto.order.OrderDTO;
import org.next.door.model.order.Order;

/**
 * @author ad
 *
 */
public final class OrderConverter {
	
	private OrderConverter () {}
	
	public static OrderDTO fromOrderToOrderDTO (Order order) {
		
		return OrderDTO.builder()
				.amount(order.getAmount())
				.orderDate(order.getOrderDate())
				.orderId(order.getOrderId())
				.orderNumber(order.getOrderNumber())
				.build();
	}
	
	public static Order fromOrderDTOToOrder (OrderDTO orderDTO) {
		
		return Order.builder()
				.amount(orderDTO.getAmount())
				.orderDate(orderDTO.getOrderDate())
				.orderId(orderDTO.getOrderId())
				.orderNumber(orderDTO.getOrderNumber())
				.build();
	}
}
