package org.next.door.converter.orderdetail;

import org.next.door.converter.order.OrderConverter;
import org.next.door.converter.product.ProductConverter;
import org.next.door.dto.orderdetail.OrderDetailDTO;
import org.next.door.model.orderdetail.OrderDetail;

/**
 * @author ad
 *
 */
public final class OrderDetailConverter {
	
	private OrderDetailConverter () {}
	
	public static OrderDetailDTO fromOrderDetailToOrderDTO (OrderDetail orderDetail) {
		return OrderDetailDTO.builder()
				.amount(orderDetail.getAmount())
				.order(OrderConverter.fromOrderToOrderDTO(orderDetail.getOrder()))
				.orderDetailId(orderDetail.getOrderDetailId())
				.price(orderDetail.getPrice())
				.amount(orderDetail.getAmount())
				.product(ProductConverter.fromProductToProductDTO(orderDetail.getProduct()))
				.quanity(orderDetail.getQuanity())
				.build();
	}
	
	public static OrderDetail fromOrderDetailDTOToOrder (OrderDetailDTO orderDetailDTO) {
		return OrderDetail.builder()
				.amount(orderDetailDTO.getAmount())
				.order(OrderConverter.fromOrderDTOToOrder(orderDetailDTO.getOrder()))
				.orderDetailId(orderDetailDTO.getOrderDetailId())
				.price(orderDetailDTO.getPrice())
				.amount(orderDetailDTO.getAmount())
				.product(ProductConverter.fromProductDTOToProduct(orderDetailDTO.getProduct()))
				.quanity(orderDetailDTO.getQuanity())
				.build();
	}
}
