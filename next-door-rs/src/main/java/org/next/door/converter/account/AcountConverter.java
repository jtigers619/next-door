package org.next.door.converter.account;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.next.door.dto.account.AccountDTO;
import org.next.door.dto.account.AddressDTO;
import org.next.door.model.account.Account;
import org.next.door.model.account.Address;

/**
 * @author ad
 *
 */
public final class AcountConverter {
	
	private AcountConverter () {}
	
	/**
	 * Convert Account to AcountDTO.
	 * 
	 * @param account
	 * @return AcountDTO
	 */
	public static AccountDTO fromAcountToAcountDTO (Account account) {
		return AccountDTO.builder()
				.accountId(account.getAccountId())
				.email(account.getEmail())
				.active(account.isActive())
				.fullName(account.getFullName())
				.mobileNo(account.getMobileNo())
				.addresses(account.getAddresses()
						.stream()
						.map(fromAddressToAddressDTO)
						.collect(Collectors.toList()))
				.build();
	}
	
	/**
	 * Convert AccountDTO to Acount.
	 * 
	 * @param accountDTO
	 * @return AcountDTO
	 */
	public static Account fromAcountDTOToAcount (AccountDTO accountDTO) {
		List<Address> addresses = accountDTO.getAddresses()
			.stream()
			.map(fromAddressDTOToAddress)
			.collect(Collectors.toList());
		Account account =  Account.builder()
				.accountId(accountDTO.getAccountId())
				.email(accountDTO.getEmail())
				.active(accountDTO.isActive())
				.fullName(accountDTO.getFullName())
				.password(accountDTO.getPassword())
				.userRole(accountDTO.getUserRole())
				.mobileNo(accountDTO.getMobileNo())
				.addresses(addresses)
				.build();
		addresses.stream().forEach(address -> address.setAccount(account));
		return account;
	}
	
	private static Function<Address, AddressDTO> fromAddressToAddressDTO = (address) -> {
		return AddressDTO.builder()
				.address(address.getAddress())
				.addressId(address.getAddressId())
				.city(address.getCity())
				.country(address.getCountry())
				.zipCode(address.getZipCode())
				.build();
	};
	
	private static Function<AddressDTO, Address> fromAddressDTOToAddress = (addressDTO) -> {
		return Address.builder()
				.address(addressDTO.getAddress())
				.addressId(addressDTO.getAddressId())
				.city(addressDTO.getCity())
				.country(addressDTO.getCountry())
				.zipCode(addressDTO.getZipCode())
				.build();
	};
}
