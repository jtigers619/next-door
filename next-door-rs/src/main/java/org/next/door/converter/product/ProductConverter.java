package org.next.door.converter.product;

import org.next.door.dto.product.ProductDTO;
import org.next.door.model.product.Product;

/**
 * @author ad
 *
 */
public final class ProductConverter {
	
	private ProductConverter () {}
	
	public static ProductDTO fromProductToProductDTO (Product product) {
		return ProductDTO.builder()
				.productId(product.getProductId())
				.code(product.getCode())
				.name(product.getName())
				.price(product.getPrice().orElse(null))
				.image(product.getImage())
				.build();
	}
	
	public static Product fromProductDTOToProduct (ProductDTO product) {
		return Product.builder()
				.productId(product.getProductId())
				.code(product.getCode())
				.name(product.getName())
				.price(product.getPrice())
				.image(product.getImage())
				.build();
	}
}
