package org.next.door.converter.cart;

import java.util.stream.Collectors;

import org.next.door.converter.product.ProductConverter;
import org.next.door.dto.cart.CartDTO;
import org.next.door.model.cart.Cart;

/**
 * @author ad
 *
 */
public final class CartConverter {
	
	private CartConverter () {}
	
	public static CartDTO fromCartToCartDTO (Cart cart) {
		return CartDTO.builder()
				.cartAmount(cart.getCartAmount().orElse(null))
				.cartId(cart.getCartId())
				.products(cart.getProducts()
							.stream().map(product -> ProductConverter.fromProductToProductDTO(product)).collect(Collectors.toList()))
				.build();
	}
	
	public static Cart fromCartDTOToCart (CartDTO cartDTO) {
		return Cart.builder()
				.cartAmount(cartDTO.getCartAmount())
				.cartId(cartDTO.getCartId())
				.products(cartDTO.getProducts()
							.stream().map(productDTO -> ProductConverter.fromProductDTOToProduct(productDTO)).collect(Collectors.toList()))
				.build();
	}
}
